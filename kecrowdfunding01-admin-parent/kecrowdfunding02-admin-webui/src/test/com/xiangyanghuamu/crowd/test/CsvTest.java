package com.xiangyanghuamu.crowd.test;

import com.opencsv.CSVReader;
import com.xiangyanghuamu.crowd.entity.Admin;
import com.xiangyanghuamu.crowd.mapper.AdminMapper;
import com.xiangyanghuamu.crowd.service.api.AdminService;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class CsvTest {

    @Autowired
    public AdminService adminService;
    @Test
    public void test(){
        try {
            String finalPath = "/Library/idea-maven-workspace/kecrowdfunding01-admin-parent/kecrowdfunding02-admin-webui/src/main/webapp/csv/44cac097b3984367b77683581084bc0e.csv";
            CSVReader csvReader = new CSVReader(new FileReader(finalPath));
            String[] titles = csvReader.readNext();// 读取到第一行，是小标题

            List<Admin> adminList = new ArrayList<>();
            Admin admin = null;
            while(true){
                admin = new Admin();
                String[] content = csvReader.readNext();
                if(content == null){
                    break;
                }
                admin.setLoginAcct(content[0]);
                admin.setUserPswd(content[1]);
                admin.setUserName(content[2]);
                admin.setEmail(content[3]);
                adminList.add(admin);
            }
            adminService.saveAll(adminList);
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test2() throws Exception {
        SqlSession sqlSession = SqlSessionUtil.getSqlSession();
        AdminMapper mapper = sqlSession.getMapper(AdminMapper.class);
        String finalPath = "/Library/idea-maven-workspace/kecrowdfunding01-admin-parent/kecrowdfunding02-admin-webui/src/main/webapp/csv/44cac097b3984367b77683581084bc0e.csv";
        CSVReader csvReader = new CSVReader(new FileReader(finalPath));
        String[] titles = csvReader.readNext();// 读取到第一行，是小标题

        List<Admin> adminList = new ArrayList<>();
        Admin admin = null;
        while (true) {
            admin = new Admin();
            String[] content = csvReader.readNext();
            if (content == null) {
                break;
            }
            admin.setLoginAcct(content[0]);
            admin.setUserPswd(content[1]);
            admin.setUserName(content[2]);
            admin.setEmail(content[3]);
            adminList.add(admin);
        }
            int res = mapper.insertBatch(adminList);
            System.out.println(res);

    }
}
