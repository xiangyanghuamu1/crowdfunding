package com.xiangyanghuamu.crowd.test;

import com.xiangyanghuamu.crowd.entity.Admin;
import com.xiangyanghuamu.crowd.entity.Role;
import com.xiangyanghuamu.crowd.mapper.AdminMapper;
import com.xiangyanghuamu.crowd.mapper.RoleMapper;
import com.xiangyanghuamu.crowd.service.api.AdminService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

// 指定Spring 给Junit 提供的运行器类
@RunWith(SpringJUnit4ClassRunner.class)
// 加载Spring 配置文件的注解
@ContextConfiguration(locations = {"classpath:spring-persist-mybatis.xml","classpath:spring-persist-tx.xml"})
public class CrowdTest {
    @Autowired
    private DataSource dataSource;
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private AdminService adminService;
    @Autowired
    private RoleMapper roleMapper;


    @Test
    public void testPassword(){
        BCPassword bcPassword = new BCPassword();
        String encode = bcPassword.encode("123123");
        System.out.println(encode);

    }

    @Test
    public void testRole(){
        for(int i = 0;i < 235;i++){
            roleMapper.insert(new Role(null,"role"+i));
        }
    }

    @Test
    public void test(){
        for(int i = 0;i< 238;i++){
            adminMapper.insert(new Admin(null,"loginAcct"+i,"userPswd"+i,"userName"+i,"email"+i,null));
        }
    }

    @Test
    public void testTx(){
        Admin admin = new Admin(null, "jerry", "12345", "杰瑞", "jerry@qq.com", null);
        adminService.saveAdmin(admin);
    }

    @Test
    public void testInsertAdmin() {
        Admin admin = new Admin(null,"tom","123123","汤姆","tom@qq.com",null);
        int count = adminMapper.insert(admin);
        System.out.println("插入行数： " + count);
    }

    @Test
    public void testDataSource() throws SQLException {
        // 1.通过数据源对象获取数据源连接
        Connection connection = dataSource.getConnection();
        // 2.打印数据库连接
        System.out.println("连接已获取"+connection);
    }
}

