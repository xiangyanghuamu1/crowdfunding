<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="zh-CN">
<%@include file="/WEB-INF/include-head.jsp"%>
<link rel="stylesheet" href="css/pagination.css" />
<script type="text/javascript" src="jquery/jquery.pagination.js"></script>
<script type="text/javascript"></script>
<body>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="panel panel-default">
					<div class="panel-body">
    <form action="admin/up/csv/page.html" method="post" enctype="multipart/form-data">
    <input type="file" name="csv">
    <input type="submit" value="CSV取込">
    </form>
<!-- <a href="admin/update/csv/page.html">登録</a> -->
    <input type="button" value="登録" onClick="location.href='admin/update/csv/page.html'"/>
		<p>${requestScope.exception.message }</p>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>