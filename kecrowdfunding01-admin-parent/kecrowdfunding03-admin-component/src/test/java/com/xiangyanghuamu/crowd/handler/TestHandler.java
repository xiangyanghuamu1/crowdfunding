package com.xiangyanghuamu.crowd.handler;

import com.xiangyanghuamu.crowd.entity.Admin;
import com.xiangyanghuamu.crowd.service.api.AdminService;
import com.xiangyanghuamu.crowd.util.CrowdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class TestHandler {

    @Autowired
    private AdminService adminService;

    private Logger logger = LoggerFactory.getLogger(TestHandler.class);


    @ResponseBody
    @RequestMapping("/send/array.html")
    public String testReceiveArrayOne(@RequestBody Integer[] array) {
        for (Integer number : array) {
            System.out.println("number = " + number);
        }
        return "success";
    }
    @RequestMapping("/test/ssm.html")
    public String testSsm(ModelMap modelMap, HttpServletRequest request){

        boolean judgeResult = CrowdUtil.judgeRequestType(request);
        logger.info("judgeResult=" + judgeResult);
        List<Admin> adminList = adminService.getAll();
        modelMap.addAttribute("adminList",adminList);
        System.out.println(10/0);
        return "target";
    }
}
