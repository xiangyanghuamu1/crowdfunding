package com.xiangyanghuamu.crowd.mvc.handler;

import com.github.pagehelper.PageInfo;
import com.opencsv.CSVReader;
import com.xiangyanghuamu.crowd.constant.CrowdConstant;
import com.xiangyanghuamu.crowd.entity.Admin;
import com.xiangyanghuamu.crowd.service.api.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class AdminHandler {

    @Autowired
    private AdminService adminService;

    @RequestMapping("/admin/update/csv/page.html")
    public String Csv(HttpSession session) throws Exception {

            String finalPath = (String)session.getAttribute("finalPath");
            if(finalPath == null){
                throw new RuntimeException(CrowdConstant.MESSAGE_ADDRESS_EMPTY);
            }
            CSVReader csvReader = new CSVReader(new FileReader(finalPath));
            String[] titles = csvReader.readNext();// 读取到第一行，是小标题

            List<Admin> adminList = new ArrayList<>();
            Admin admin = null;
            while(true){
                admin = new Admin();
                String[] content = csvReader.readNext();
                if(content == null){
                    break;
                }
                admin.setLoginAcct(content[0]);
                admin.setUserPswd(content[1]);
                admin.setUserName(content[2]);
                admin.setEmail(content[3]);
                adminList.add(admin);
            }
            adminService.saveAll(adminList);
            csvReader.close();
        return "redirect:/admin/get/page.html";
    }

    @RequestMapping("/admin/up/csv/page.html")
    public String upCsv(MultipartFile csv, HttpSession session) throws Exception{

                if(csv.getOriginalFilename().equals("")){
                    throw new FileNotFoundException(CrowdConstant.MESSAGE_ADDRESS_EMPTY);
                }


                //获取上传的文件的文件名
                String fileName = csv.getOriginalFilename();
                //获取上传的文件的后缀名
                String suffixName = fileName.substring(fileName.lastIndexOf("."));
                //将UUID作为文件名
                String uuid = UUID.randomUUID().toString().replaceAll("-","");
                //将uuid和后缀名拼接后的结果作为最终的文件名
                fileName = uuid + suffixName;
                //通过ServletContext获取服务器中csv文件的路径
                ServletContext servletContext = session.getServletContext();
                String csvPath = servletContext.getRealPath("csv");
                File file = new File(csvPath);
                //判断photoPath所对应路径是否存在
                if(!file.exists()){
                    //若不存在，则创建目录
                    file.mkdir();
                }
                String finalPath = csvPath + File.separator + fileName;
                //上传文件
                csv.transferTo(new File(finalPath));
                session.setAttribute("finalPath",finalPath);

                if(finalPath != null){
                        throw new FileNotFoundException(CrowdConstant.MESSAGE_FILE_SUCCESS);
                }

                return "csv-page";
        }


    @RequestMapping("/admin/update.html")
    public String update(
            Admin admin,
            @RequestParam("pageNum")Integer pageNum,
            @RequestParam("keyword")String keyword
    ){
        adminService.update(admin);
        return "redirect:/admin/get/page.html?pageNum="+pageNum+"&keyword"+keyword;
    }

    @RequestMapping("/admin/to/edit/page.html")
    public String toUpdate(
            @RequestParam("adminId") Integer adminId,
            ModelMap modelMap
    ){
        Admin admin = adminService.getAdminById(adminId);
        modelMap.addAttribute(admin);
        return "admin-edit";

    }

    @PreAuthorize("hasAuthority('user:save')")
    @RequestMapping("/admin/save.html")
    public String save(Admin admin){
        adminService.saveAdmin(admin);
        return "redirect:/admin/get/page.html?pageNum="+Integer.MAX_VALUE;
    }

    @RequestMapping("/admin/remove/{adminId}/{pageNum}/{keyword}.html")
    public String remove(
            @PathVariable Integer adminId,
            @PathVariable Integer pageNum,
            @PathVariable String keyword
    ){
        //执行删除
        adminService.remove(adminId);
        //重定向到admin/get/page.html地址
        //同时为了保持原本所在的页面和查询关键词再附加pageNum和keyword两个请求参数
        return "redirect:/admin/get/page.html?pageNum="+pageNum+"&keyword"+keyword;
    }


    @RequestMapping("/admin/get/page.html")
    public String getPageInfo(
            @RequestParam(value="keyword",defaultValue = "")String keyword,
            @RequestParam(value="pageNum",defaultValue = "1")Integer pageNum,
            @RequestParam(value="pageSize",defaultValue = "5")Integer pageSize,
            ModelMap modelMap
    ){
        PageInfo<Admin> pageInfo = adminService.getPageInfo(keyword, pageNum, pageSize);
        modelMap.addAttribute(CrowdConstant.ATTR_NAME_PAGE_INFO,pageInfo);
        return "admin-page";
    }

    @RequestMapping("/admin/do/logout.html")
    public String doLogout(HttpSession session){
        //强制session失效
        session.invalidate();
        return "redirect:/admin/to/login/page.html";
    }
    @RequestMapping("/admin/do/login.html")
    public String doLogin(
                          @RequestParam("loginAcct")String loginAcct,
                          @RequestParam("userPswd")String userPswd,
                          HttpSession session
                       ){

        //调用Service方法执行登录检查
        Admin admin = adminService.getAdminByLoginAcct(loginAcct,userPswd);
        //将Admin对象放入Session域
        session.setAttribute(CrowdConstant.LOGIN_ADMIN_NAME, admin);
        //重定向到登录完成后的主页面（重定向防止重复提交表单，增加不必要的数据库访问）
        return "redirect:/admin/to/main/page.html";
    }


}
