package com.xiangyanghuamu.crowd.mvc.config;

import com.xiangyanghuamu.crowd.entity.Admin;
import com.xiangyanghuamu.crowd.entity.Role;
import com.xiangyanghuamu.crowd.service.api.AdminService;
import com.xiangyanghuamu.crowd.service.api.AuthService;
import com.xiangyanghuamu.crowd.service.api.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CrowdUserDetailsService implements UserDetailsService {

    @Autowired
    private AdminService adminService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private AuthService authService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        // 1.根据账号名称查询Admin对象
        Admin admin = adminService.getAdminByLoginAcct(username);

        // 2.获取adminId
        Integer adminId = admin.getId();

        // 3.根绝adminId查询角色信息
        List<Role> assignedRoleList = roleService.getAssignedRole(adminId);

        // 4.根据adminId查询权限信息
        List<String> authNameList = authService.getAssignedAuthNameByAdminId(adminId);

        // 5.创建集合对象用来存储GrantedAuthority
        List<GrantedAuthority> Authorities = new ArrayList<>();

        // 6.遍历assignedRoleList存入角色信息
        for(Role role : assignedRoleList){

            // 注意不要忘了加前缀
            String roleName = "ROLE_" + role.getName();

            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(roleName);

            Authorities.add(simpleGrantedAuthority);
        }

        // 7.遍历authNameList存入权限信息
        for(String authName : authNameList){

            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(authName);

            Authorities.add(simpleGrantedAuthority);
        }

        // 8.封装securityAdmin对象，返回
        SecurityAdmin securityAdmin = new SecurityAdmin(admin, Authorities);

        return securityAdmin;
    }
}
