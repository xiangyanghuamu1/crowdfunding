package com.xiangyanghuamu.crowd.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiangyanghuamu.crowd.constant.CrowdConstant;
import com.xiangyanghuamu.crowd.entity.Admin;
import com.xiangyanghuamu.crowd.entity.AdminExample;
import com.xiangyanghuamu.crowd.exception.LoginAcctAlreadyInUseException;
import com.xiangyanghuamu.crowd.exception.LoginAcctAlreadyInUseForUpdateException;
import com.xiangyanghuamu.crowd.exception.LoginFailedException;
import com.xiangyanghuamu.crowd.mapper.AdminMapper;
import com.xiangyanghuamu.crowd.service.api.AdminService;
import com.xiangyanghuamu.crowd.util.CrowdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminMapper adminMapper;
    private Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Override
    public List<Admin> getAll() {
        return adminMapper.selectByExample(new AdminExample());
    }

    @Override
    public Admin getAdminByLoginAcct(String loginAcct, String userPswd) {
        // 1.使用账号从数据库获取Admin对象
        // ①获取AdminExample对象
        AdminExample adminExample = new AdminExample();
        // ②创建Criteria对象
        AdminExample.Criteria criteria = adminExample.createCriteria();
        // ③在Criteria对象中封装查询条件
        criteria.andLoginAcctEqualTo(loginAcct);
        // ④调用AdminMapper的方法执行查询
        List<Admin> list = adminMapper.selectByExample(adminExample);
        // 2.判断Admin对象是否为null,是否存在重复数据
        if(list == null || list.size()==0){
            // 3.如果Admin对象为null抛出异常
            throw new LoginFailedException(CrowdConstant.MESSAGE_LOGIN_FAILED);
        }

        if(list.size() > 1){
            throw new LoginFailedException(CrowdConstant.MESSAGE_SYSTEM_ERROR_LOGIN_NOT_UNIQUE);
        }
        // 4.如果Admin对象不为null，从获取的对象中取出密码
        Admin admin = list.get(0);
        String userPswdDB = admin.getUserPswd();
        // 5.用MD5对表单密码进行加密
        String userPswdForm = CrowdUtil.md5(userPswd);
        // 6.对比数据库密码与表单密码
        if(!Objects.equals(userPswdDB,userPswdForm)){
            // 7.如果不一致抛异常
            throw new LoginFailedException(CrowdConstant.MESSAGE_LOGIN_FAILED);
        }
        // 8.如果一致返回Admin对象
        return admin;
    }

    @Override
    public PageInfo<Admin> getPageInfo(String keyword, Integer pageNum, Integer pageSize) {

        // 1.调用PageHelper的静态方法开启分页功能
        PageHelper.startPage(pageNum,pageSize);

        // 2.执行查询
        List<Admin> list = adminMapper.selectAdminByKeyword(keyword);

        // 3.封装到PageInfo对象中
        return new PageInfo<>(list);
    }

    @Override
    public void remove(Integer adminId) {
        adminMapper.deleteByPrimaryKey(adminId);
    }

    @Override
    public Admin getAdminById(Integer adminId) {
        Admin admin = adminMapper.selectByPrimaryKey(adminId);
        return admin;
    }

    @Override
    public void saveAll(List<Admin> adminList) {

        //生成创建时间
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String createTime = simpleDateFormat.format(date);

        for(Admin admin : adminList){
            //密码加密
            String userPswd = admin.getUserPswd();
            userPswd = passwordEncoder.encode(userPswd);
            admin.setUserPswd(userPswd);
            admin.setCreateTime(createTime);
        }

        //执行保存
        try {
            adminMapper.insertBatch(adminList);
        } catch (Exception e) {
            e.printStackTrace();
            if(e instanceof DuplicateKeyException){
                throw new LoginAcctAlreadyInUseException(CrowdConstant.MESSAGE_LOGIN_ACCT_ALREADY_IN_USE);
            }
        }
    }

    @Override
    public void saveAdmin(Admin admin) {
        //密码加密
        String userPswd = admin.getUserPswd();
//        userPswd = CrowdUtil.md5(userPswd);
        userPswd = passwordEncoder.encode(userPswd);
        admin.setUserPswd(userPswd);
        //生成创建时间
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String createTime = simpleDateFormat.format(date);
        admin.setCreateTime(createTime);
        //执行保存
        try {
            adminMapper.insert(admin);
        } catch (Exception e) {
            e.printStackTrace();
            if(e instanceof DuplicateKeyException){
                throw new LoginAcctAlreadyInUseException(CrowdConstant.MESSAGE_LOGIN_ACCT_ALREADY_IN_USE);
            }


        }
    }

    @Override
    public void update(Admin admin) {
        try {
            adminMapper.updateByPrimaryKeySelective(admin);
        } catch (Exception e) {
            if(e instanceof DuplicateKeyException){
                throw new LoginAcctAlreadyInUseForUpdateException(CrowdConstant.MESSAGE_LOGIN_ACCT_ALREADY_IN_USE);
            }
        }
    }

    @Override
    public void saveAdminRoleRelationship(Integer adminId, List<Integer> roleIdList) {

        // 为了简化操作，先根据adminId删除旧的数据，再根据roleIdList保存全部新的数据
        // 1.根据adminId删除旧的数据
        adminMapper.deleteOldRelationship(adminId);

        if(roleIdList != null && roleIdList.size() > 0){

            // 2.根据roleIdList保存全部新的数据
            adminMapper.insertNewRelationship(adminId,roleIdList);

        }

    }

    @Override
    public Admin getAdminByLoginAcct(String username) {
        AdminExample adminExample = new AdminExample();
        AdminExample.Criteria criteria = adminExample.createCriteria();
        criteria.andLoginAcctEqualTo(username);
        List<Admin> list = adminMapper.selectByExample(adminExample);
        Admin admin = list.get(0);
        return admin;
    }
}
