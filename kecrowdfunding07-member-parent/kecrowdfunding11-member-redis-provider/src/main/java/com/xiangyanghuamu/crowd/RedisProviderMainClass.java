package com.xiangyanghuamu.crowd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class RedisProviderMainClass {
    public static void main(String[] args) {
        SpringApplication.run(RedisProviderMainClass.class, args);
    }
}