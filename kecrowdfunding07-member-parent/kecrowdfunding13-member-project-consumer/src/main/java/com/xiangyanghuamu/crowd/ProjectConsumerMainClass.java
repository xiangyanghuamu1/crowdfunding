package com.xiangyanghuamu.crowd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Hello world!
 *
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class ProjectConsumerMainClass {
    public static void main(String[] args) {
        SpringApplication.run(ProjectConsumerMainClass.class, args);
    }
}