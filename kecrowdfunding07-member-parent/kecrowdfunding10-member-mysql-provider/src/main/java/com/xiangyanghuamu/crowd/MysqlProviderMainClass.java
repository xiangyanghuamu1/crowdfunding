package com.xiangyanghuamu.crowd;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@MapperScan("com.xiangyanghuamu.crowd.mapper")
@SpringBootApplication
public class MysqlProviderMainClass {
    public static void main(String[] args) {
        SpringApplication.run(MysqlProviderMainClass.class, args);
    }
}