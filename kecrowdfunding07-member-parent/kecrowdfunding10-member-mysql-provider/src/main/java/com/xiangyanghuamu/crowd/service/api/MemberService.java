package com.xiangyanghuamu.crowd.service.api;

import com.xiangyanghuamu.crowd.entity.po.MemberPO;

public interface MemberService {
    MemberPO getMemberPOByLoginAcct(String loginacct);

    void saveMember(MemberPO memberPO);
}
