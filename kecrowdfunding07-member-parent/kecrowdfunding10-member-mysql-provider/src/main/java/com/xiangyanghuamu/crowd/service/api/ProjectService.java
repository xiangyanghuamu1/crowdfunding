package com.xiangyanghuamu.crowd.service.api;

import com.xiangyanghuamu.crowd.entity.vo.DetailProjectVO;
import com.xiangyanghuamu.crowd.entity.vo.PortalTypeVO;
import com.xiangyanghuamu.crowd.entity.vo.ProjectVO;

import java.util.List;

public interface ProjectService {
    void saveProject(ProjectVO projectVO, Integer memberId);

    List<PortalTypeVO> getPortalTypeVO();

    DetailProjectVO getDetailProjectVO(Integer projectId);
}
