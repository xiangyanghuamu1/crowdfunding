package com.xiangyanghuamu.crowd;


import com.aliyun.api.gateway.demo.util.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CrowdTest{

    private Logger logger = LoggerFactory.getLogger(CrowdTest.class);
    @Test
    public void testSendMessage(){

            String host = "https://dfsmsv2.market.alicloudapi.com";
            String path = "/data/send_sms_v2";
            String method = "POST";
            String appcode = "1d962e8e4258422d8b28f179d9722c49";
            Map<String, String> headers = new HashMap<String, String>();
            //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
            headers.put("Authorization", "APPCODE " + appcode);
            //根据API的要求，定义相对应的Content-Type
            headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            Map<String, String> querys = new HashMap<String, String>();
            Map<String, String> bodys = new HashMap<String, String>();
            // 要发送的验证码
            bodys.put("content", "code:9999");
            // 收短信的手机号
            bodys.put("phone_number","17623240705");
            // 模版
            bodys.put("template_id", "TPL_0000");//


            try {
                /**
                 * 重要提示如下:
                 * HttpUtils请从
                 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
                 * 下载
                 *
                 * 相应的依赖请参照
                 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
                 */
                HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);

                StatusLine statusLine = response.getStatusLine();

                // 状态码：200：正常 400：URL无效 401：appCode错误 403:次数用完 500:API网管错误
                int statusCode = statusLine.getStatusCode();
                logger.info("code="+statusCode);

                String reasonPhrase = statusLine.getReasonPhrase();
                logger.info("reason="+reasonPhrase);
                //获取response的body
                logger.info(EntityUtils.toString(response.getEntity()));
            } catch (Exception e) {
                e.printStackTrace();
            }

    }
}
